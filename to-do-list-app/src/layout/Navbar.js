import React from 'react';
import PropTypes from 'prop-types';
import {Link} from 'react-router-dom';

const Navbar = (props) => {
    return (
        <nav className="navbar-nav navbar-expand-lg navbar-dark bg-dark mb-3">
            <a href="/" className="navbar-brand">{props.title}</a>
            <ul className="navbar-nav ml-auto">
                <li className="nav-item active">
                    <Link to="/" className="nav-link">Home</Link>
                </li>
                <li className="nav-item active">
                    <Link to="/addtask" className="nav-link">Add Task</Link>
                </li>
                <li className="nav-item active">
                    <Link to="/register" className="nav-link">Register</Link>
                </li>
                <li className="nav-item active">
                    <Link to="/login" className="nav-link">Login</Link>
                </li>
            </ul>
        </nav>
    )
}
Navbar.propTypes = {
    title: PropTypes.string.isRequired
}
export default Navbar;


