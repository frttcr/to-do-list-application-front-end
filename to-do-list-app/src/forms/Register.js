import React, { Component } from 'react';
import RegisterConsumer from '../context';
import axios from 'axios';

class Register extends Component {

    state = {
        email: "",
        username: "",
        password: ""
    }

    changeInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    registerUser = async (dispatch, e) => {
        e.preventDefault();
        const {email, username, password} = this.state;

        const newUser = {
            email: email,
            username: username,
            password: password
        }

        const response = await axios.post("http://localhost:8080/api/register", newUser);

        dispatch({
            type: "REGISTER_USER",
            payload: response.data
        });
    }

    render() {
        const {email, username, password} = this.state;
        
        return <RegisterConsumer>
            {
                value => {
                    const {dispatch} = value;
                    return (
                        <div className="col-md-10 offset-md-1 mb-4">
                            <div className="card">
                                    <div className="card-header">
                                        <h4>Register User</h4>
                                    </div>
                                    <div className="card-body">
                                        <form onSubmit={this.registerUser.bind(this, dispatch)}>
                                            <div className="form-group">
                                                <label htmlFor="email">Email:</label>
                                                <input type="email" name="email" id="email" placeholder="Enter Email Address" 
                                                    className="form-control" value={email} onChange={this.changeInput} />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="username">Username:</label>
                                                <input type="text" name="username" id="username" placeholder="Enter Username" 
                                                    className="form-control" value={username} onChange={this.changeInput} />
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="password">Password:</label>
                                                <input type="password" name="password" id="password" placeholder="Enter Password" 
                                                    className="form-control" value={password} onChange={this.changeInput} />
                                            </div>
                                            <button type="submit" className="btn btn-success btn-block">Register</button>
                                        </form>
                                    </div>
                                </div>
                        </div>
                    )
                }
            }
        </RegisterConsumer>
    }
}
export default Register;