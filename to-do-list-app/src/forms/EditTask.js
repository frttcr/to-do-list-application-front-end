import React, { Component } from 'react';
import TaskConsumer from '../context';
import axios from 'axios';

class EditTask extends Component {
    currentTime = () => {
        let current_datetime = new Date();
        let formatted_date = current_datetime.getFullYear() + "-" + (current_datetime.getMonth() + 1) + "-" + current_datetime.getDate() + "T" + (current_datetime.getHours() - 1) + ":" + current_datetime.getMinutes() + ":" + current_datetime.getSeconds();
        return formatted_date;
    }

    state = {
        name: "",
        description: "",
        createdDate: this.currentTime(),
        deadline: "",
        isCompleted: "Not Completed",
        isExpired: "False",
        error: false
    }

    changeInput = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    componentDidMount = async () => {
        const {id} = this.props.match.params;

        const response = await axios.get(`http://localhost:8080/api/testtask/${id}`);

        const {name, description, createdDate, deadline, isCompleted, isExpired} = response.data;
        
        this.setState({
            name,
            description,
            createdDate,
            deadline,
            isCompleted,
            isExpired
        })
    }

    validateForm = () => {
        const {name, description, deadline, isCompleted, isExpired} = this.state;

        if(name === "" || description === "" || deadline === "" || isCompleted === "" || isExpired === "") {
            return false;
        }
        return true;
    }

    editTask = async (dispatch, e) => {
        e.preventDefault();
        const { name, description, deadline, isCompleted, isExpired } = this.state;
        const { id } = this.props.match.params;

        const updatedTask = {
            name: name,
            description: description,
            deadline: deadline,
            isCompleted: isCompleted,
            isExpired: isExpired
        }

        if(!this.validateForm()) {
            this.setState({
                error: true
            })
            return;
        }
        
        const response = await axios.put(`http://localhost:8080/api/task/update/${id}`, updatedTask);

        dispatch({
            type: "EDIT_TASK",
            payload: response.data
        });

        // Redirect
        this.props.history.push("/");
    }

    render() {
        const { name, description, createdDate, deadline, isCompleted, isExpired, error } = this.state;

        return <TaskConsumer>
            {
                value => {
                    const { dispatch } = value;
                    return (
                        <div className="col-md-10 offset-md-1 mb-4">
                            <div className="card">
                                <div className="card-header">
                                    <h4>Edit Task</h4>
                                </div>
                                <div className="card-body">
                                    {
                                        error ? 
                                        <div className="alert alert-danger">
                                            Please enter all informations.
                                        </div>
                                        :
                                        null
                                    }
                                    <form onSubmit={this.editTask.bind(this, dispatch)}>
                                        <div className="form-group">
                                            <label htmlFor="name">Name:</label>
                                            <input type="text" name="name" id="name" placeholder="Enter the task name"
                                                className="form-control" value={name} onChange={this.changeInput} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="name">Description:</label>
                                            <input type="text" name="description" id="description" placeholder="Enter the description of the task"
                                                className="form-control" value={description} onChange={this.changeInput} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="name">Created Date:</label>
                                            <input type="datetime-local" name="deadline" id="deadline" placeholder="Enter the deadline for the task"
                                                className="form-control" value={createdDate} onChange={this.changeInput} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="name">Deadline:</label>
                                            <input type="datetime-local" name="deadline" id="deadline" placeholder="Enter the deadline for the task"
                                                className="form-control" value={deadline} onChange={this.changeInput} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="name">Status:</label>
                                            <input type="text" name="isCompleted" id="isCompleted" placeholder="Enter the status for the task"
                                                className="form-control" value={isCompleted} onChange={this.changeInput} />
                                        </div>
                                        <div className="form-group">
                                            <label htmlFor="name">Expired:</label>
                                            <input type="text" name="isExpired" id="isExpired" placeholder="Enter the expired for the task"
                                                className="form-control" value={isExpired} onChange={this.changeInput} />
                                        </div>
                                        <button type="submit" className="btn btn-success btn-block">Save</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    )
                }
            }
        </TaskConsumer>
    }
}
export default EditTask;