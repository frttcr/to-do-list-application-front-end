import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TaskConsumer from '../context';
import axios from 'axios';
import {Link} from 'react-router-dom';

class Task extends Component{
    
    static defaultProps = {
        name : "No Information",
        description : "No Information",
        deadline : "No Information",
    }

    constructor(props) {
        super(props);

        this.state = {
            isVisible : false
        }
    }

    onClickEvent = (e) => {
        this.setState({
            isVisible : !this.state.isVisible
        })
    }

    onDeleteTask = async (dispatch, e) => {
        const {id} = this.props;

        // Delete Request
        await axios.delete(`http://localhost:8080/api/testtask/delete/${id}`);

        // Consumer Dispatch
        dispatch({type: "DELETE_TASK", payload: id});
    }

    render() {
         // Destructing
        const {id, name, description, createdDate, deadline, isCompleted, isExpired} = this.props;
        const {isVisible} = this.state;

        return (
            <TaskConsumer>
            {
                value => {
                    const {dispatch} = value;

                    return (
                        <div className="col-md-10 offset-md-1 mb-4">
                            <div className="card">
                                <div className="card-header d-flex justify-content-between">
                                    <h4 className="d-inline" onClick={this.onClickEvent}>{name}</h4>
                                    <i onClick={this.onDeleteTask.bind(this, dispatch)} className="far fa-trash-alt" style = {{cursor : "pointer"}}></i>
                                </div>
                                {
                                    isVisible ? 
                                    <div className="card-body">
                                        <p className="card-text">Description: {description}</p>
                                        <p className="card-text">Created Date: {createdDate}</p>
                                        <p className="card-text">Deadline: {deadline}</p>
                                        <p className="card-text">Status: {isCompleted}</p>
                                        <p className="card-text">Expired: {isExpired}</p>
                                        <Link to={`edittask/${id}`} className="btn btn-dark btn-block">Edit Task</Link>
                                    </div> : null   
                                }
                                
                            </div>
                        </div>
                    )
                }        
            }
            </TaskConsumer>
        )
    }
}
Task.propTypes = {  
    name : PropTypes.string.isRequired,
    description : PropTypes.string.isRequired,   
    deadline : PropTypes.string.isRequired,
    isCompleted : PropTypes.string.isRequired,   
    isExpired : PropTypes.string.isRequired,
    createdDate : PropTypes.string.isRequired,
    id : PropTypes.string.isRequired
}
export default Task;