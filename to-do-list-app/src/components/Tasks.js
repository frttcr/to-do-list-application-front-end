import React, { Component } from 'react';
import Task from "./Task";
import TaskConsumer from '../context';

class Tasks extends Component {
    render() {
        return(
            <TaskConsumer>
                {
                    value => {
                        const {tasks} = value;

                        return (
                            <div>
                                {
                                    tasks.map(task => {
                                        return (
                                            <Task
                                                key = {task.id}
                                                id = {task.id}
                                                name = {task.name}
                                                description = {task.description}
                                                createdDate = {task.createdDate}
                                                deadline = {task.deadline}
                                                isCompleted = {task.isCompleted}
                                                isExpired = {task.isExpired}
                                            />
                                        )
                                    })
                                }
                            </div>
                        )
                    }
                }
            </TaskConsumer>
        )
    }
}
export default Tasks;