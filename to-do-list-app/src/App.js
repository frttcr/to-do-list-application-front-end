import React, {Component} from 'react';
import './App.css';
import Navbar from './layout/Navbar';
import Register from './forms/Register';
import Login from './components/Login';
import Tasks from './components/Tasks';
import AddTask from './forms/AddTask';
import EditTask from './forms/EditTask';
import NotFound from './pages/NotFound';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

class App extends Component {
  render() {
    return (
      <Router>
        <div className="container">
          <Navbar title="Task App"/>
          <hr/> 
          <Switch>
            <Route exact path = "/" component = {Tasks}/>
            <Route exact path = "/addtask" component = {AddTask}/>
            <Route exact path = "/register" component = {Register}/>
            <Route exact path = "/login" component = {Login}/>
            <Route exact path = "/edittask/:id" component = {EditTask} />
            <Route component = {NotFound} />
          </Switch>

          
        </div>
      </Router>
    ) 
  }
}
export default App;