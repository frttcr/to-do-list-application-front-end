import React, { Component } from 'react';
import axios from 'axios';

const TaskContext = React.createContext();
// Provider, Consumer
const reducer = (state, action) => {
    switch(action.type) {
        case "DELETE_TASK":
            return {
                ...state, // Old state
                tasks: state.tasks.filter(task => action.payload !== task.id)
            }
        case "ADD_TASK":
            return {
                ...state,
                tasks: [...state.tasks, action.payload]
            }
        case "EDIT_TASK":
            return {
                ...state,
                tasks: state.tasks.map(task => task.id === action.payload.id ? action.payload : task)
            }
        default:
            return state
    }
}

export class TaskProvider extends Component {
    state = {
        tasks: [],
        dispatch : action => {
            this.setState(state => reducer(state, action))
        }
    }

    componentDidMount = async () => {
        const response = await axios.get("http://localhost:8080/api/testtasks");
        console.log(response);
        this.setState({
            tasks: response.data
        })
    }

    render() {
        return (
            <TaskContext.Provider value = {this.state}>
                {this.props.children}
            </TaskContext.Provider>
        )
    }
}
const TaskConsumer = TaskContext.Consumer;
export default TaskConsumer;

